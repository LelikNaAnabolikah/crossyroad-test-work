﻿Shader "Unlit/Border"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_LeftBorder("Left",float) = -15
		_RightBorder("Right",float) = 15
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 200
		 Lighting On
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD2;

			};

			float4 _Color;
			float _LeftBorder;
			float _RightBorder;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 col = _Color;
			if (i.worldPos.z < _LeftBorder || i.worldPos.z > _RightBorder)
			{
				col *= 0.55;
			}

				return col;
			}
			ENDCG
		}
	}
}
