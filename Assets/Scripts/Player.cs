﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class Player : MonoBehaviour
{
    public enum Swipe
    {
        None,
        Up,
        Down,
        Left,
        Right
    };

    public class moveMent
    {
        public DelegateMove fun;
        public Vector3 rotate = new Vector3();

        public moveMent(DelegateMove fun, Vector3 rotate)
        {
            this.fun = fun;
            this.rotate = rotate;
        }
    }

    
    //----------REFS------------------
    public TerrainManager TerrainManager;
    public DieManager DieManager;
    public GameManager GameManager;
    public AudioSource JumpAudio;
    public GameObject Skin;
    private Animator animator;

    public Line currentLine;
    public Line newLine;
    public bool haveParent = false;
    private Log parentLog;
    private int localIndex;

    public delegate Vector3 DelegateMove();
    public List<moveMent> movements = new List<moveMent>();

    //--------BOOLS-------
    private bool isJumping = false;
    public bool isDie = false;
    public bool soundOn = true;



    
    
    
    public float minSwipeLength = 0.2f;
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    public static Swipe swipeDirection;

    public bool debugWithArrowKeys = true;

    Vector2 startPos;
    float startTime;

    private Vector3 futurePos;
    private void Start()
    {
        animator = GetComponent<Animator>();
        newLine = currentLine;
        GoToGrass(transform.position + new Vector3(-1, 0, 0));
    }

    private void Update()
    {

        if (Input.touches.Length > 0 && !isDie)
        {
            Touch t = Input.GetTouch(0);

            if (t.phase == TouchPhase.Began)
            {
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }

            if (t.phase == TouchPhase.Ended)
            {
                secondPressPos = new Vector2(t.position.x, t.position.y);
                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                
                if (currentSwipe.magnitude < minSwipeLength)
                {
                    
                    GameManager.GameStarted();
                    movements.Add(new moveMent(MoveForward, new Vector3(0, 90, 0)));
                    Move();
                    if (futurePos != transform.position)
                    {
                        GameManager.Forward();
                    }
                    return;
                }

                currentSwipe.Normalize();

                
                if (currentSwipe.y > 0 &&  currentSwipe.x > -0.5f && currentSwipe.x < 0.5f) {
                    GameManager.GameStarted();
                    movements.Add(new moveMent(MoveForward, new Vector3(0, 90, 0)));
                    Move();
                    if (futurePos != transform.position)
                    {
                        GameManager.Forward();
                    }
                    
                } else if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f) {
                    movements.Add(new moveMent(MoveBack, new Vector3(0, -90, 0)));
                    Move();
                    if (futurePos != transform.position) GameManager.Back();

                    
                }
                else if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f) {
                    movements.Add(new moveMent(MoveLeft, new Vector3(0, 0, 0)));
                    Move();
                    
                } else if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f) {
                    
                    movements.Add(new moveMent(MoveRight, new Vector3(0, 180, 0)));
                    Move();
                }
            }
        }
        else
        {
            swipeDirection = Swipe.None;
        }
        
        if (Input.GetKeyDown(KeyCode.W) && !isDie)
        {
            GameManager.GameStarted();
            movements.Add(new moveMent(MoveForward, new Vector3(0, 90, 0)));
            Move();
            if (futurePos != transform.position)
            {
                GameManager.Forward();
            }
        }
        else if (Input.GetKeyDown(KeyCode.A)  && !isDie)
        {
            movements.Add(new moveMent(MoveLeft, new Vector3(0, 0, 0)));
            Move();
        }
        else if (Input.GetKeyDown(KeyCode.D) && !isDie)
        {
            movements.Add(new moveMent(MoveRight, new Vector3(0, 180, 0)));
            Move();
        }
        else if (Input.GetKeyDown(KeyCode.S) && !isDie)
        {
            movements.Add(new moveMent(MoveBack, new Vector3(0, -90, 0)));
            Move();
            if (futurePos != transform.position) GameManager.Back();
            
        }

        if ((transform.position.z < -15 || transform.position.z > 15) && haveParent) Drow();
        /*if (transform.position.y < -2)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<Rigidbody>().useGravity = false;
        }*/
    }
    
    public void Restart()
    {
        transform.DOTogglePause();
        movements.Clear();
        isDie = false;
        transform.localScale = new Vector3(1, 1, 1);
    }

    private void Move()
    {
        if (!isJumping&&movements.Count!=0)
        {
            
            Sequence mySequence = DOTween.Sequence();
            Vector3 difference = movements[0].fun();
            Vector3 rotate = movements[0].rotate;

            //animator.SetTrigger("Jump");
            //isJumping = true;
            //(Vector3 endValue, float jumpPower, int numJumps, float duration, bool snapping)

            isJumping = true;
            if (haveParent)
            {
                Skin.transform.DORotate(rotate, 0.1f);
                mySequence.Join(transform.DOLocalJump(difference, 0.5f, 1, 0.3f, false)).SetEase(Ease.Linear).OnComplete(FinishJump);
            }
            else
            {
                Skin.transform.DORotate(rotate, 0.1f);
                mySequence.Join(transform.DOJump(difference, 0.5f, 1, 0.3f, false)).SetEase(Ease.Linear).OnComplete(FinishJump);
            }
            movements.Remove(movements[0]);

            if(soundOn)JumpAudio.Play();
        }
    }
    

    public void FinishJump()
    {
        isJumping = false;
        transform.localPosition = new Vector3(futurePos.x, transform.localPosition.y, futurePos.z);
        if (parentLog != null) parentLog.Anim();
        Move();
    }

    public Vector3 MoveForward()
    {
        Vector3 futureposition = transform.position;

        newLine = currentLine.nextLine;
        switch (newLine.typeOfLine)
        {
            case TypeOfLine.Grass:
                futureposition = GoToGrass(transform.position + new Vector3(1, 0, 0));
                break;
            case TypeOfLine.Road:
                futureposition = GoToGrass(transform.position + new Vector3(1, 0, 0));
                currentLine = newLine;
                break;
            case TypeOfLine.Water:
                futureposition = FindLogTile(transform.position + new Vector3(1, 0, 0));
                break;
            case TypeOfLine.Train:
                futureposition = GoToGrass(transform.position + new Vector3(1, 0, 0));
                break;
        }
        futurePos = futureposition;
        return CheckBorder(futureposition);
    }

    public Vector3 MoveBack()
    {
        Vector3 futureposition = transform.position;

        newLine = currentLine.previousLine;

        switch (newLine.typeOfLine)
        {
            case TypeOfLine.Grass:
                futureposition = GoToGrass(transform.position + new Vector3(-1, 0, 0));
                break;
            case TypeOfLine.Road:
                futureposition = GoToGrass(transform.position + new Vector3(-1, 0, 0));
                currentLine = newLine;
                break;
            case TypeOfLine.Water:
                futureposition = FindLogTile(transform.position+new Vector3(-1, 0, 0));
                break;
            case TypeOfLine.Train:
                futureposition = GoToGrass(transform.position + new Vector3(-1, 0, 0));
                break;
        }
        futurePos = futureposition;
        return CheckBorder(futureposition);
    }

    public Vector3 MoveRight()
    {
        Vector3 futureposition = transform.position;

        
        switch (currentLine.typeOfLine)
        {
            case TypeOfLine.Grass:
                futureposition = transform.position + new Vector3(0, 0, -1);
                if (currentLine.CheckTile(Mathf.RoundToInt(transform.position.z - 1)))
                {
                    futureposition = transform.position;

                }
                break;
            case TypeOfLine.Road:
                futureposition = transform.position + new Vector3(0, 0, -1);
                if (currentLine.CheckTile(Mathf.RoundToInt(transform.position.z - 1)))
                {
                    futureposition = transform.position;

                }
                break;
            case TypeOfLine.Water:
                futureposition = SideTile(transform.position + new Vector3(0, 0, -1f),-1);
                break;
            case TypeOfLine.Train:
                futureposition = transform.position + new Vector3(0, 0, -1);
                if (currentLine.CheckTile(Mathf.RoundToInt(transform.position.z - 1)))
                {
                    futureposition = transform.position;

                }
                break;
        }
        futurePos = futureposition;
        return CheckBorder(futureposition);
    }

    public Vector3 MoveLeft()
    {
        Vector3 futureposition = transform.position;

        switch (currentLine.typeOfLine)
        {
            case TypeOfLine.Grass:
                futureposition = transform.position + new Vector3(0, 0, 1);
                if (currentLine.CheckTile(Mathf.RoundToInt(transform.position.z + 1)))
                {
                    futureposition = transform.position;

                }
                break;
            case TypeOfLine.Road:
                futureposition = transform.position + new Vector3(0, 0, 1);
                if (currentLine.CheckTile(Mathf.RoundToInt(transform.position.z + 1)))
                {
                    futureposition = transform.position;

                }
                break;
            case TypeOfLine.Water:
                futureposition = SideTile(transform.position + new Vector3(0, 0, 1f), 1);
                break;
            case TypeOfLine.Train:
                futureposition = transform.position + new Vector3(0, 0, 1);
                if (currentLine.CheckTile(Mathf.RoundToInt(transform.position.z + 1)))
                {
                    futureposition = transform.position;

                }
                break;
        }
        futurePos = futureposition;
        return CheckBorder(futureposition);
    }
    

    public Vector3 FindLogTile(Vector3 futurePos)
    {
        LineWater line = (LineWater)newLine;
        foreach (Log log in line.pullOfLogs)
        {
            foreach (logTile tile in log.tiles)
            {
                float difference = 10;
                difference = Mathf.Abs((log.transform.position.z + tile.localPosition.z) - futurePos.z);
                if (difference <= 1.3f)
                {
                    transform.SetParent(log.transform);
                    haveParent = true;
                    parentLog = log;
                    currentLine = newLine;
                    localIndex = log.tiles.IndexOf(tile);
                    
                    return  tile.localPosition;
                }
            }
        }
        if (haveParent)
            transform.SetParent(null);
        isDie = true;
        transform.DOLocalJump(futurePos+new Vector3(0,0.3f,0), 0.5f, 1, 0.3f, false)
            .OnComplete(SecondLife);
        
        return futurePos;
    }

    public void SecondLife()
    {
        LineWater line = (LineWater)newLine;
        foreach (Log log in line.pullOfLogs)
        {
            foreach (logTile tile in log.tiles)
            {
                float difference = 10;
                difference = Mathf.Abs((log.transform.position.z + tile.localPosition.z) - futurePos.z);
                if (difference <= 1f && parentLog != log)
                {
                    transform.SetParent(log.transform);
                    haveParent = true;
                    isDie = false;
                    parentLog = log;
                    currentLine = newLine;
                    localIndex = log.tiles.IndexOf(tile);
                    //transform.localPosition = tile.localPosition;
                    transform.DOLocalMove(tile.localPosition, 0.2f, false);
                    return;
                }
            }
        }
        
        DieManager.Drowned();

    }

    public Vector3 SideTile(Vector3 futurePos, int nextIndex)
    {
        localIndex += nextIndex;

        if (localIndex < 0 || localIndex > parentLog.tiles.Count - 1)
        {
            if (haveParent)
                transform.SetParent(null);

            FindLogTile(futurePos + new Vector3(0, 0, nextIndex));
            
        }
        else futurePos = parentLog.tiles[localIndex].localPosition;
        return futurePos;
    }

    public Vector3 GoToGrass(Vector3 futurePos)
    {
        
        if (newLine.CheckTile(Mathf.RoundToInt(transform.position.z)))
        {
            if (haveParent) return parentLog.tiles[localIndex].localPosition;
            return transform.position;

        }
        currentLine = newLine;
        if (haveParent)
            transform.SetParent(null);
        haveParent = false;
        return futurePos;
    }

    public Vector3 CheckBorder(Vector3 futurepos)
    {
        if (futurepos.z > 15 || futurePos.z < -15)
        {
            futurePos = transform.position;
            return transform.position;
        }
        return futurePos;
    }

    public void Drow()
    {
        if (!isDie)
        {
            DieManager.DrownedLog(parentLog);
            GetComponent<Collider>().enabled = false;
        }
    }
}
