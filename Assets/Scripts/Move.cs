﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Move : MonoBehaviour
{
    public bool isMoovable = true;
    public Vector3 finishPosition = new Vector3(0, 0, 0);
    public void move()
    {
         isMoovable = false;
         DOTween.Complete(transform, false);
    }
}
