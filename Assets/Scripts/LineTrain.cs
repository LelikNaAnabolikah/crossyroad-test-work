﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineTrain : Line, forLines
{
    public override void SetPositionsOfTiles()
    {

        float startPos;
        float endPos;
        bool Boolean = (Random.Range(0, 2) == 1);
        if (Boolean)
        {
            startPos = -30;
            endPos = 30;
        }
        else
        {
            startPos = 30;
            endPos = -30;
        }

        pullOfBarriers[currentBarrier].transform.position = new Vector3(currentPosition.x, 1f, startPos);
        pullOfBarriers[currentBarrier].GetComponent<Train>().SetInfo(new Vector3(currentPosition.x, 1f, startPos), new Vector3(currentPosition.x, 1f, endPos), prephab);

    }
}
