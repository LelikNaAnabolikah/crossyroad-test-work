﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Icon : MonoBehaviour
{
    [SerializeField] private AnimationEditor AnimationEditor;


    public void ShowEnd()
    {
        AnimationEditor.IconShowed();
    }


    public void HideEnd()
    {
        AnimationEditor.IconHidden();
    }
}
