﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Signs : MonoBehaviour
{
    public List<GameObject> redSigns = new List<GameObject>();
    public List<GameObject> greenSigns = new List<GameObject>();
}
