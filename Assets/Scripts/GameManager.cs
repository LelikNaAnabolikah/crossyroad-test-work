﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.SceneManagement;
using DG.Tweening;
public enum Status
{
    START_MENU, 
    GAME_MENU,
    RESULT_MENU,
}

public class GameManager : MonoBehaviour
{
    private Status status = Status.START_MENU;


    [Serializable]
    class Save
    {
        public int coins;
        public int score;
        public bool soundOn;

        public void SetSave(int coins, int score, bool soundOn)
        {
            this.coins = coins;
            this.score = score;
            this.soundOn = soundOn;
        }
    }

    private int Score = 0;
    private int highScore = 0;
    private int Coins = 0;
    private int minusScore = 0;
    private bool soundOn = false;

    private Save save = new Save();
    
    private string path;

    [SerializeField] private TerrainManager terrainManager;
    [SerializeField] private DieManager DieManager;
    [SerializeField] private AnimationEditor AnimationEditor;
    [SerializeField] private FollowCamera FollowCamera;
    [SerializeField] private UIManager uIManager;


    //--------------SOUND--------------------------------------------
    [SerializeField] private Texture2D off;
    [SerializeField] private Texture2D on;
    

    public void Start()
    {
        AnimationEditor.BackExecute("Hide");

        GetSave();
        highScore = save.score;
        Coins = save.coins;
        soundOn = save.soundOn;

        SetCoinsScore();
        SetScore();
        SetSound();
        
    }

    private Save GetSave()
    {
        int sound = PlayerPrefs.GetInt("sound");
        save.soundOn = sound == 0 ? false : true;
        save.coins = PlayerPrefs.GetInt("coins");
        save.score = PlayerPrefs.GetInt("score");
        highScore = save.score;
        soundOn = save.soundOn;

/*#if UNITY_ANDROID && !UNITY_EDITOR
        path = Path.Combine(Application.persistentDataPath, "Save.json");
#else
        path = Path.Combine(Application.dataPath, "Save.json");
#endif
        if (File.Exists(path))
        {
            save = JsonUtility.FromJson<Save>(File.ReadAllText(path));
        }
        else Debug.Log("nothing");
        */
        return save;
    }

    private void OnApplicationQuit()
    {
        SetSave();
    }

    private void SetSave()
    {
        int sound = 0;
        if(save.soundOn) sound = 1;
        PlayerPrefs.SetInt("sound", sound);
        PlayerPrefs.SetInt("coins", save.coins);
        PlayerPrefs.SetInt("score", save.score);
        save.SetSave(Coins, highScore, soundOn);
        //File.WriteAllText(path, JsonUtility.ToJson(save));
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name.Contains("Coin"))
        {
            terrainManager.CoinBack(collider.gameObject);
            Coins++;
            SetCoinsScore();
            SetSave();
        }

    }

    private void SetCoinsScore()
    {
        uIManager.SetCoinsScore(Coins);
    }

    private void SetScore()
    {
        uIManager.SetScore(Score);
    }

    private void SetHighScore()
    {
        uIManager.SetHighScore(Score);
    }

    public void Forward()
    {
        if (minusScore == 0)
        {
            Score++;
            terrainManager.LinePlus();
        }
        else minusScore++;
        SetScore();
    }

    public void Back()
    {
        minusScore--;
    }

    public void LoseGame()
    {
        if (Score > highScore) highScore = Score;
        Score = 0;
        SetHighScore();

        uIManager.LoseGame(highScore);
        AnimationEditor.ScoreScale();
        SetSave();
        FollowCamera.isActive = false;
        Fun();
        
    }

    private void Fun()
    {
        FollowCamera.StartCoroutine("MoveToSpot");
    }
    public void Restart()
    {
        SetSave();
        uIManager.Restart();
        
        //terrainManager.Restart();
        AnimationEditor.IconExecute("Show");
        
    }

    public void GameStarted()
    {
        AnimationEditor.IconExecute("Hide");
    }
    
    public void LoadScene()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void Sound()
    {
        soundOn = !soundOn;
        SetSound();
    }

    void SetSound()
    {
        if (soundOn) uIManager.SetSound(on);
        else uIManager.SetSound(off);
        DieManager.Player.soundOn = soundOn;
        SetSave();
    }
}
