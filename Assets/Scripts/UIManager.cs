﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    //---------------Game menu --------------------------------------------
    [SerializeField] private GameObject GameMenu;
    [SerializeField] private Text coins1;
    [SerializeField] private Text score1;

    //--------------Result menu --------------------------------------------
    [SerializeField] private GameObject ResultMenu;
    [SerializeField] private Text coins3;
    [SerializeField] private Text score3;
    [SerializeField] private Text highScore3;
    [SerializeField] private RawImage soundImage;


    int highScore = 0;

    public void SetCoinsScore(int Coins)
    {
        coins1.text = Coins.ToString();
        coins3.text = Coins.ToString();
    }

    public void SetScore(int Score)
    {
        score1.text = Score.ToString();
        score3.text = Score.ToString();
    }

    public void SetHighScore(int highScore)
    {
        highScore3.text = highScore.ToString();
    }

    public void Restart()
    {
        ResultMenu.SetActive(false);
        GameMenu.SetActive(true);

    }

    public void LoseGame(int Score)
    {
        if (Score > highScore) highScore = Score;
        SetHighScore(highScore);

        GameMenu.SetActive(false);
        ResultMenu.SetActive(true);

    }

    public void SetSound(Texture2D image)
    {
        soundImage.texture = image;
    }
}
