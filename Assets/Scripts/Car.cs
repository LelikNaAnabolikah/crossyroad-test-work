﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Car : Move
{
    public Vector3 startPosition = new Vector3(0, 0, 0);
    //public Vector3 finishPosition = new Vector3(0, 0, 0);
    private float speed;
    public void StartMove()
    {
        if (isMoovable)
        {
            transform.DOTogglePause();
            transform.position = startPosition;
            transform
                .DOMove(finishPosition, speed, false)
                .OnComplete(StartMove);
        }
    }



    public void SetInfo(Vector3 startPosition, Vector3 finishPosition, float startTime, float speed, int direction)
    {
        isMoovable = true;
        this.startPosition = startPosition;
        this.finishPosition = finishPosition;
        this.speed = speed;
        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y + direction, transform.rotation.z);
        transform
            .DOMove(finishPosition, startTime, false)
            .OnComplete(StartMove);  
    }
}
