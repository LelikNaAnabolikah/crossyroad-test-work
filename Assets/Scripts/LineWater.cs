﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineWater : Line, forLines
{
    public List<Log> pullOfLogs = new List<Log>();
    public override void SetPositionsOfTiles()
    {
        
        float startPos;
        float endPos;
        int direction = 0;
        bool Boolean = (Random.Range(0, 2) == 1);
        if (Boolean)
        {
            startPos = -30;
            endPos = 30;
            direction = 20;
        }
        else
        {
            startPos = 30;
            endPos = -30;
            direction = 0;
        }
        int count = Random.Range(6, 9);
        float step = (60) / count;
        float stepTime = 20 / count;
        int speed;

        bool b1 = (Random.Range(0, 2) == 1);
        if(b1) speed = 15+ Random.Range(0, 4);
        else speed = 20;
        for (int i = 0; i < count; i++)
        {
            if (currentBarrier < pullOfBarriers.Count)
            {
                pullOfBarriers[currentBarrier].transform.position = new Vector3(currentPosition.x, 0.5f, -30 + step * i);
                pullOfBarriers[currentBarrier].GetComponent<Log>().SetInfo(new Vector3(currentPosition.x, 0.5f, startPos), new Vector3(currentPosition.x, 0.5f, endPos), (20 / count) * Mathf.Abs(i - direction) + 4, speed);
                pullOfLogs.Add(pullOfBarriers[currentBarrier].GetComponent<Log>());
                currentBarrier++;
            }
        }
    }
}
