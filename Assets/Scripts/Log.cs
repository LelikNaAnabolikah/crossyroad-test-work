﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class logTile
{
    public Vector3 localPosition = new Vector3(0,0,0);
    public Transform parent;
    

    public logTile(Vector3 newPos, Transform parent)
    {
        localPosition = newPos;
        this.parent = parent;
    }
}


public class Log : Move
{
    
    public GameObject prefab;
    public int numberOfTiles = 0;

    public Vector3 startPosition = new Vector3(0, 0, 0);
    //public Vector3 finishPosition = new Vector3(0, 0, 0);
    public List<logTile> tiles = new List<logTile>();
    public float size = 0;
    public float speed = 0;
    public Animator anim;

    public void StartMove()
    {
        if (isMoovable)
        {
            transform.DOTogglePause();
            transform.position = startPosition;
            transform.DOMove(finishPosition, speed, false).OnComplete(StartMove);
        }
    }

    

    public List<logTile> SetInfo(Vector3 startPosition, Vector3 finishPosition, float startTime, float speed)
    {
        isMoovable = true;
        
        this.startPosition = startPosition;
        this.finishPosition = finishPosition;
        this.speed = speed;

        size = Random.Range(2, 5);
        prefab.transform.localScale = new Vector3(0.6f,1,size);
        float startPos = -(size / 2 - 0.5f);
        for(int i = 0; i < size; i++)
        {
            tiles.Add(new logTile( new Vector3(0, 1, startPos + (float)i), transform));
        }


        transform
            .DOMove(finishPosition, startTime, false)
            .OnComplete(StartMove);
            
        return tiles;
    }

    public void Die()
    {
        isMoovable = false;
        transform.DOPause();
        //DOTween.Complete(transform, false);
        transform
            .DOLocalMove(new Vector3(transform.localPosition.x, -2f, finishPosition.z*0.8f), 3f, false);
            
    }

    public void Anim()
    {
        anim.SetTrigger("Play");
    }
}
