﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Back : MonoBehaviour
{
    [SerializeField] private AnimationEditor AnimationEditor;


    public void ShowEnd()
    {
        AnimationEditor.BackShowed();
    }


    public void HideEnd()
    {
        AnimationEditor.BackHidden();
    }
}
