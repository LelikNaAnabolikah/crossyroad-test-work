﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Train : Move
{

    public Vector3 startPosition = new Vector3(0, 0, 0);
    public float speed = 2;
    private float delay = 0;
    private List<GameObject> greenSigns = new List<GameObject>();
    private List<GameObject> redSigns = new List<GameObject>();


    public void StartMove()
    {
        if (isMoovable)
        {
            transform.DOTogglePause();
            transform.position = startPosition;
            transform.DOMove(finishPosition, speed, false)
                .OnComplete(Fun);
        }
    }

    public void SetInfo(Vector3 startPosition, Vector3 finishPosition, GameObject prephab)
    {
        isMoovable = true;
        delay = Random.Range(0, 7);
        this.startPosition = startPosition;
        this.finishPosition = finishPosition;

        redSigns = prephab.GetComponent<Signs>().redSigns;
        greenSigns = prephab.GetComponent<Signs>().greenSigns;




        Fun();

    }


    public void Fun()
    {
        foreach (GameObject red in redSigns) red.SetActive(false);
        foreach (GameObject green in greenSigns) green.SetActive(true);
        StartCoroutine("DelayMove");
    }
    IEnumerator DelayMove()
    {
        
        yield return new WaitForSeconds(5+delay);
        foreach(GameObject red in redSigns)red.SetActive(true);
        foreach (GameObject green in greenSigns) green.SetActive(false);
        
        StartCoroutine("DelaySign");
        


    }

    IEnumerator DelaySign()
    {
        yield return new WaitForSeconds(2);
        StartMove();
    }


}
