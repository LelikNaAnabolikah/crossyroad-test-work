﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public DieManager dieManager;

    public Transform target;
    Vector3 dif;
    public float maxSpeed;
    public float horizontalSpeed;
    public float time;
    public bool isActive = true;
    
    public float offset;
    private Vector3 velocity = Vector3.zero;
    private Camera camera;
    void Start()
    {
        camera = GetComponent<Camera>();
        transform.position = new Vector3(3, 18, -7.5f);
        dif = target.transform.position - transform.position;
    }
    

    private void FixedUpdate()
    {
        if (isActive)
        {
            float hor = Mathf.Lerp(transform.position.z, target.position.z, horizontalSpeed*10 * Time.deltaTime);
            transform.position = new Vector3(transform.position.x, transform.position.y, hor);
            Vector3 desiredPos = Vector3.Lerp(target.position, target.position + new Vector3(10, 0, -5), offset);

            Vector3 smothPosition = Vector3.SmoothDamp(new Vector3(transform.position.x, transform.position.y, hor), desiredPos, ref velocity, time, maxSpeed);

            transform.position = smothPosition;

            if (Vector3.Distance(new Vector3(transform.position.x, transform.position.y, hor), desiredPos) < 12f)
            {
                dieManager.Die(DieManager.typeOfDie.TIME_LEFT);
            }

        }
        
    }

    IEnumerator MoveToSpot()
    {
        
        float start = 8.5f;
        float end = 7f;
        float elapsedTime = 0;
        float waitTime = 1.4f;
        Vector3 pos = transform.position;
        Vector3 endpos = target.position - dif;
        while (elapsedTime < waitTime)
        {
            camera.orthographicSize = Mathf.Lerp(start, end, (elapsedTime / waitTime));
            transform.position = Vector3.Lerp(pos, endpos, (elapsedTime / waitTime));
            elapsedTime += Time.deltaTime;

            
            yield return null;
        }
        
        yield return null;
    }
}
