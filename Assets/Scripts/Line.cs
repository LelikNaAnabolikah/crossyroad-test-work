﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line 
{
    public TerrainManager TerrainManager;
    
    public Vector3 currentPosition;

    public Line previousLine;
    public Line nextLine;
    public TypeOfLine typeOfLine;
    public List<Tile> tiles = new List<Tile>();
    
    public GameObject prephab;
    
    public List<GameObject> pullOfBarriers = new List<GameObject>();
    public int currentBarrier=0;

    
    public virtual void SetPositionsOfTiles() {}

    public void AddTiles()
    {
       
        for(int i = -16; i<16; i++)
        {
            tiles.Add(new Tile(i, false));
        }
    }

    public bool CheckTile(int index)
    {

        if (FindTileByIndex(index).haveBarrier)
            return true; 
        return false;
    }

    public Tile FindTileByIndex(int index)
    {
        Tile foundedTile = new Tile(-20, false);
        foreach(Tile tile in tiles)
        {
            if (tile.index == index)
            {
                
                foundedTile = tile;
            }
        }
        return foundedTile;
    }
}
