﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineGrass : Line, forLines
{
    public override void SetPositionsOfTiles()
    {
        for(int i = -16; i < 16; i++)
        {
            if (currentBarrier < tiles.Count-1)
            {
                bool Boolean = (Random.Range(0, 14) == 1);

                if (Boolean || i == -15 || i == 15)
                {
                    tiles[currentBarrier].haveBarrier = true;

                    pullOfBarriers[currentBarrier].transform.position = new Vector3(currentPosition.x, 0.5f, i);

                }
                else if (Random.Range(0, 100) == 1)
                {
                    TerrainManager.UseCoin().transform.position = new Vector3(currentPosition.x, 0.5f, i);

                }
                currentBarrier++;
            }
            
        }
    }
}
