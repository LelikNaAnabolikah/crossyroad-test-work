﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TerrainManager : MonoBehaviour
{

    [SerializeField] private Player player;
    
    [SerializeField] private Transform pullOfWater;
    [SerializeField] private Transform pullOfGrass;
    [SerializeField] private Transform pullOfRoad;
    [SerializeField] private Transform pullOfTrain;
    private List<Line> spawnedGrass = new List<Line>();
    private List<Line> spawnedRoad = new List<Line>();
    private List<Line> spawnedWater = new List<Line>();
    private List<Line> spawnedTrain = new List<Line>();
    [HideInInspector] public List<GameObject> spawnedCoins = new List<GameObject>();
    [HideInInspector] public List<GameObject> usedCoins = new List<GameObject>();
    [SerializeField] private GameObject coinPrephab;

    private Vector3 currentPosiition = new Vector3(0, 0, 0);

    [SerializeField] private List<TileInfo> tiles = new List<TileInfo>();

    
    private List<Line> LinesInMap = new List<Line>();

    private int stepsToNext = 0;
    private int lineCounter = 9;
    private TypeOfLine currentTypeOfLine = TypeOfLine.Grass;
    private void Start()
    {
        SpawnPullOfLines();
        SpawnPullOfCoins();

        stepsToNext = 10;
        LinesInMap.Add(spawnedGrass[0]);
        spawnedGrass.Remove(spawnedGrass[0]);

        GererateTerrain(50);
        
        player.currentLine = LinesInMap[9];
        player.transform.position = new Vector3(LinesInMap[9].currentPosition.x, 2, -2);
        
    }

    public void Restart()
    {
        Debug.Log("Restart");
        currentPosiition = new Vector3(0, 0, 0);
        foreach (Line line in LinesInMap)
        {

            line.prephab.transform.position = new Vector3(0, 0, 0);
            foreach (GameObject barriers in line.pullOfBarriers)
            {
                //barriers.transform.DOTogglePause();
                //barriers.transform.DOKill();
                //barriers.transform.DoRestart();
                //barriers.transform.DORewind();
                barriers.transform.position = new Vector3(0, -20, -40);
                barriers.transform.DOComplete(false);
                barriers.GetComponent<Move>().move();
                //barriers.GetComponent<Move>().finishPosition = new Vector3(0, -20, -40);
                //barriers.transform.DOMove(new Vector3(0, -20, -41), 0.1f, true);
            }

            switch (line.typeOfLine)
            {
                case TypeOfLine.Grass:
                    spawnedGrass.Add(line);
                    break;
                case TypeOfLine.Road:
                    spawnedRoad.Add(line);
                    break;
                case TypeOfLine.Water:
                    spawnedWater.Add(line);
                    break;
                case TypeOfLine.Train:
                    spawnedTrain.Add(line);
                    break;
            }

            
        }
        
        LinesInMap.Clear();
        //Debug.Log("Spawned: " + spawnedGrass.Count + " " + spawnedRoad.Count + " " + spawnedWater.Count + "Used: " + LinesInMap.Count);


        stepsToNext = 10;
        currentTypeOfLine = TypeOfLine.Grass;
        stepsToNext = 10;
        LinesInMap.Add(spawnedGrass[0]);
        spawnedGrass.Remove(spawnedGrass[0]);

        GererateTerrain(50);

        player.Restart();
        player.currentLine = LinesInMap[9];
        
        player.transform.position = new Vector3(LinesInMap[9].currentPosition.x, 1, -2);

    }

    public void LinePlus()
    {
        lineCounter++;

        if (lineCounter > 14)
        {
            switch (LinesInMap[0].typeOfLine)
            {
                case TypeOfLine.Grass:
                    spawnedGrass.Add(LinesInMap[0]);
                    break;
                case TypeOfLine.Road:
                    spawnedRoad.Add(LinesInMap[0]);
                    break;
                case TypeOfLine.Water:
                    spawnedWater.Add(LinesInMap[0]);
                    break;
                case TypeOfLine.Train:
                    spawnedTrain.Add(LinesInMap[0]);
                    break;
            }
            
            LinesInMap.Remove(LinesInMap[0]);
        }
        //Debug.Log("Spawned: " + spawnedGrass.Count+ " " + spawnedRoad.Count+" " + spawnedWater.Count + "Used: " +LinesInMap.Count);
        GererateTerrain(1);
    }

    public void GererateTerrain(int numberOfLines)
    {
        for (int j = 0; j < numberOfLines; j++)
        {
            if (stepsToNext == 0)
            {
                int whatLine = Random.Range(0, tiles.Count);
                int numberOflines = Random.Range(tiles[whatLine].minInSuccession, tiles[whatLine].maxInSuccession);
                stepsToNext = numberOflines;
                currentTypeOfLine = tiles[whatLine].typeOfLine;

                switch (currentTypeOfLine)
                {
                    case TypeOfLine.Grass:
                        if (stepsToNext >spawnedGrass.Count)
                        {
                            if (stepsToNext < spawnedRoad.Count) currentTypeOfLine = TypeOfLine.Road;
                            if (stepsToNext < spawnedWater.Count) currentTypeOfLine = TypeOfLine.Water;
                        }
                            break;
                    case TypeOfLine.Road:
                        if (stepsToNext > spawnedRoad.Count)
                        {
                            if (stepsToNext < spawnedGrass.Count) currentTypeOfLine = TypeOfLine.Grass;
                            if (stepsToNext < spawnedWater.Count) currentTypeOfLine = TypeOfLine.Water;
                        }
                        break;
                    case TypeOfLine.Water:
                        if (stepsToNext > spawnedWater.Count)
                        {
                            if (stepsToNext < spawnedRoad.Count) currentTypeOfLine = TypeOfLine.Road;
                            if (stepsToNext < spawnedGrass.Count) currentTypeOfLine = TypeOfLine.Grass;
                        }
                        break;
                    
                }
            }

            for (int i = 0; i < stepsToNext; i++)
            {
                switch (currentTypeOfLine)
                {
                    
                    case TypeOfLine.Grass:
                        LinesInMap.Add(spawnedGrass[spawnedGrass.Count - 1]);
                        spawnedGrass.Remove(spawnedGrass[spawnedGrass.Count - 1]);                        
                        break;
                    case TypeOfLine.Road:
                        LinesInMap.Add(spawnedRoad[spawnedRoad.Count - 1]);
                        spawnedRoad.Remove(spawnedRoad[spawnedRoad.Count - 1]);
                        break;
                    case TypeOfLine.Water:
                        LinesInMap.Add(spawnedWater[spawnedWater.Count - 1]);
                        spawnedWater.Remove(spawnedWater[spawnedWater.Count - 1]);
                        break;
                    case TypeOfLine.Train:
                        LinesInMap.Add(spawnedTrain[spawnedTrain.Count - 1]);
                        spawnedTrain.Remove(spawnedTrain[spawnedTrain.Count - 1]);
                        break;
                }
                LinesInMap[LinesInMap.Count - 1].prephab.transform.position = currentPosiition;
                LinesInMap[LinesInMap.Count - 1].currentPosition = currentPosiition;
                currentPosiition = currentPosiition + new Vector3(1, 0, 0);
                LinesInMap[LinesInMap.Count - 1].SetPositionsOfTiles();
                //Debug.Log("index:"+(LinesInMap.Count - 2));
                LinesInMap[LinesInMap.Count - 1].previousLine = LinesInMap[LinesInMap.Count - 2];
                LinesInMap[LinesInMap.Count - 2].nextLine = LinesInMap[LinesInMap.Count - 1];
                
                j++;
                stepsToNext--;
            }
        }
    }
    

    public void SpawnPullOfLines()
    {
        for(int i =0; i<tiles.Count; i++)
        {
            for (int j = 0; j < 50; j++)
            {
                switch (tiles[i].typeOfLine)
                {
                    case TypeOfLine.Grass:
                        LineGrass newGrass = new LineGrass();
                        newGrass.TerrainManager = this;
                        newGrass.prephab = Instantiate(tiles[i].tile, pullOfGrass);
                        newGrass.pullOfBarriers = SpawnPullOfBarriers(tiles[i].barriers, tiles[i].numberOfBarriers, newGrass.prephab.transform);
                        newGrass.typeOfLine = TypeOfLine.Grass;
                        
                        newGrass.AddTiles();
                        spawnedGrass.Add(newGrass);
                        break;
                    case TypeOfLine.Road:
                        LineRoad newRoad = new LineRoad();
                        newRoad.TerrainManager = this;
                        newRoad.prephab = Instantiate(tiles[i].tile, pullOfRoad);
                        newRoad.pullOfBarriers = SpawnPullOfBarriers(tiles[i].barriers, tiles[i].numberOfBarriers, newRoad.prephab.transform);
                        newRoad.typeOfLine = TypeOfLine.Road;
                        
                        newRoad.AddTiles();
                        spawnedRoad.Add(newRoad);
                        break;
                    case TypeOfLine.Water:
                        LineWater newWater = new LineWater();
                        newWater.TerrainManager = this;
                        newWater.prephab = Instantiate(tiles[i].tile, pullOfWater);
                        newWater.pullOfBarriers = SpawnPullOfBarriers(tiles[i].barriers, tiles[i].numberOfBarriers, newWater.prephab.transform);
                        newWater.typeOfLine = TypeOfLine.Water;
                        
                        newWater.AddTiles();
                        spawnedWater.Add(newWater);
                        break;
                    case TypeOfLine.Train:
                        LineTrain newTrain = new LineTrain();
                        newTrain.TerrainManager = this;
                        newTrain.prephab = Instantiate(tiles[i].tile, pullOfTrain);
                        newTrain.pullOfBarriers = SpawnPullOfBarriers(tiles[i].barriers, tiles[i].numberOfBarriers, newTrain.prephab.transform);
                        newTrain.typeOfLine = TypeOfLine.Train;

                        newTrain.AddTiles();
                        spawnedTrain.Add(newTrain);
                        break;
                }
            }
        }
        
    }

    public List<GameObject> SpawnPullOfBarriers(List<GameObject> barriers, int numberOfTiles, Transform parent)
    {
        List<GameObject> newPullOfTiles = new List<GameObject>();
        for (int i = 0; i < numberOfTiles - 1; i++)
        {
            GameObject newBarrier = Instantiate(barriers[Random.Range(0, barriers.Count)],new Vector3(0,-20,0),Quaternion.identity, parent);
            newPullOfTiles.Add(newBarrier);
        }
        return newPullOfTiles;
    }

    public void SpawnPullOfCoins()
    {

        for (int i = 0; i < 10; i++)
        {
            GameObject newCoin = Instantiate(coinPrephab, new Vector3(0, -20, 0), Quaternion.identity);
            
            spawnedCoins.Add(newCoin);
        }
        
    }

    public GameObject UseCoin()
    {
        GameObject coin;
        if (spawnedCoins.Count == 0)
        {
            coin = usedCoins[0];
            usedCoins.Remove(usedCoins[0]);
        }
        else
        {
            coin = spawnedCoins[0];
            usedCoins.Add(spawnedCoins[0]);
            spawnedCoins.Remove(spawnedCoins[0]);
        }

        return coin;
    }

    public void CoinBack(GameObject coin)
    {
        spawnedCoins.Add(coin);
        usedCoins.Remove(coin);
        coin.transform.position = new Vector3(0, 0, 0);
    }
}
