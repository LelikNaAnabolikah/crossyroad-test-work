﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TypeOfLine
{
    Grass,
    Road,
    Water,
    Train,
}

[CreateAssetMenu(fileName ="Tile Info", menuName = "Tile Info")]
public class TileInfo : ScriptableObject
{
    public TypeOfLine typeOfLine;
    public GameObject tile;
    public List<GameObject> barriers = new List<GameObject>();
    public int numberOfBarriers;

    public int minInSuccession;
    public int maxInSuccession;
}
