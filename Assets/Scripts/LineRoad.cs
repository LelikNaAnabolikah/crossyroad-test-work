﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRoad : Line, forLines
{
    public override void SetPositionsOfTiles()
    {
        float startPos;
        float endPos;
        int direction = 0;
        bool Boolean = (Random.Range(0, 2) == 1);
        if (Boolean)
        {
            startPos = -30;
            endPos = 30;
            direction = 0;
        }
        else
        {
            startPos = 30;
            endPos = -30;
            direction = 180;
        }
        int count = Random.Range(2, 4);
        float step = (60) / count;
        float stepTime = 20 / count;
        float speed = Random.Range(16, 24);
        for (int i = 0; i < count; i++)
        {

            if (currentBarrier < pullOfBarriers.Count)
            {
                pullOfBarriers[currentBarrier].transform.position = new Vector3(currentPosition.x, 0.5f, -30 + step * i);

                pullOfBarriers[currentBarrier].GetComponent<Car>().SetInfo(new Vector3(currentPosition.x, 0.5f, startPos), new Vector3(currentPosition.x, 0.5f, endPos), 20 / count * i, speed, direction);

                currentBarrier++;
            }
        }
    }
}
