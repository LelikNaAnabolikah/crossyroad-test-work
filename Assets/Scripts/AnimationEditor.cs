﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEditor : MonoBehaviour
{
    [SerializeField] private Animator icon;
    [SerializeField] private Animator back;
    [SerializeField] private Animator score;
    [SerializeField] private Animator eagle;
    [SerializeField] private GameManager GameManager;


    bool showed = true;

    public void IconExecute(string command)
    {
        icon.SetTrigger(command);
    }

    public void BackExecute(string command)
    {
        back.SetTrigger(command);
    }

    public void BackHidden()
    {

    }

    public void Catch()
    {
        eagle.SetTrigger("Catch");
    }
    

    public void ScoreScale()
    {
        score.SetTrigger("Open");
    }
    public void BackShowed()
    {
        GameManager.LoadScene();
    }

    public void IconHidden()
    {

    }

    public void IconShowed()
    {
        BackExecute("Show");
    }
}
