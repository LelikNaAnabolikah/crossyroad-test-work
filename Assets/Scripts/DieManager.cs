﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DieManager : MonoBehaviour
{
    public enum typeOfDie
    {
        DROWNED,
        CAR_SIDE,
        CAR_FORWARD,
        TIME_LEFT,
        TRAIN,
    }

    public delegate void OperationDelegate();
    public Dictionary<typeOfDie, OperationDelegate> operations = new Dictionary<typeOfDie, OperationDelegate>();
    private GameObject car;
    private Rigidbody rb;

    [SerializeField] public Player Player;
    [SerializeField] private GameManager GameManager;
    [SerializeField] private AnimationEditor AnimationEditor;

    private void Start()
    {
        operations.Add(typeOfDie.DROWNED, Drowned);
        operations.Add(typeOfDie.CAR_SIDE, CarSide);
        operations.Add(typeOfDie.CAR_FORWARD, CarForward);
        operations.Add(typeOfDie.TIME_LEFT, TimeLeft);
        rb = this.GetComponent<Rigidbody>();
    }


    public void Die(typeOfDie command)
    {
        operations[command]();
    }


    public void Drowned()
    {
        transform.DOLocalMove(new Vector3(transform.position.x, transform.position.y - 2, transform.position.z), 0.3f, false);
        GameManager.LoseGame();
    }

    public void CarSide()
    {
        if (!Player.isDie)
        {
            Debug.Log("CAR_SIDE");
            
            transform.position = new Vector3(transform.position.x, 0.5f, transform.position.z);
            transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y + 90f, transform.rotation.z));
            transform.localScale = new Vector3(transform.localScale.x * 1.3f, 0.1f, transform.localScale.z * 1.3f);
            Player.isDie = true;
            GameManager.LoseGame();
        }
    }

    public void CarForward()
    {

    }

    public void DrownedLog(Log log)
    {
        log.Die();
        Player.isDie = true;
        GameManager.LoseGame();
    }

    public void Train(GameObject train)
    {
        transform.SetParent(train.transform);
        Player.isDie = true;
        GameManager.LoseGame();
    }

    public void TimeLeft()
    {
        Debug.Log("TIME_LEFT");
        Player.isDie = true;
        AnimationEditor.Catch();
        GameManager.LoseGame();
    }


    public void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name.Contains("Car"))
        {
            car = collider.gameObject;
            Die(typeOfDie.CAR_SIDE);
        } 
        if (collider.gameObject.name.Contains("Train"))
        {
            Train(collider.gameObject);
        }
    }
}
