﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile 
{
    public float index;
    public bool haveBarrier = false;

    public Tile(float index, bool haveBarrier)
    {
        this.index = index;
        this.haveBarrier = haveBarrier;
    }
}
